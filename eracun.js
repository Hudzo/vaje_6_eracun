if (!process.env.PORT)
  process.env.PORT = 8080;

// Priprava povezave na podatkovno bazo
var sqlite3 = require('sqlite3').verbose();
var pb = new sqlite3.Database('chinook.sl3');

// Priprava strežnika
var express = require('express');
var expressSession = require('express-session');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));
streznik.use(
  expressSession({
    secret: '1234567890QWERTY', // Skrivni ključ za podpisovanje piškotkov
    saveUninitialized: true,    // Novo sejo shranimo
    resave: false,              // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 3600000           // Seja poteče po 1h neaktivnosti
    }
  })
);

var razmerje_usd_eur = 0.877039116;

// Prikaz seznama pesmi na strani
streznik.get('/', function(zahteva, odgovor) {
  pb.all("SELECT Genre.Name AS zanr, Track.TrackId AS id, Track.Name AS pesem, \
          Artist.Name AS izvajalec, Track.UnitPrice * " +
          razmerje_usd_eur + " AS cena, \
          COUNT(InvoiceLine.InvoiceId) AS steviloProdaj \
          FROM Track, Album, Artist, InvoiceLine, Genre \
          WHERE Track.AlbumId = Album.AlbumId AND \
          Artist.ArtistId = Album.ArtistId AND \
          InvoiceLine.TrackId = Track.TrackId AND\
          Track.GenreId = Genre.GenreId\
          GROUP BY Track.TrackId \
          ORDER BY steviloProdaj DESC, pesem ASC \
          LIMIT 100", function(napaka, vrstice) {
    if (napaka)
      odgovor.sendStatus(500);
    else
        for(var i=0; i<vrstice.length; i++){
          //var deli = vrstice[i].split([0]+' ('+[1]+ ') ('+[2]+ ') ');
          vrstice[i].stopnja = davcnaStopnja(vrstice[i].izvajalec, vrstice[i].zanr);
        }
      odgovor.render('seznam', {seznamPesmi: vrstice});
  })
})

// Dodajanje oz. brisanje pesmi iz košarice
streznik.get('/kosarica/:idPesmi', function(zahteva, odgovor) {
  var idPesmi = parseInt(zahteva.params.idPesmi);
  if (!zahteva.session.kosarica)
    zahteva.session.kosarica = [];
  if (zahteva.session.kosarica.indexOf(idPesmi) > -1) {
    zahteva.session.kosarica.splice(zahteva.session.kosarica.indexOf(idPesmi), 1);
  } else {
    zahteva.session.kosarica.push(idPesmi);
  }
  odgovor.send(zahteva.session.kosarica);
});

//(dol) tu izpisi se zanr
var davcnaStopnja = function(izvajalec, zanr){
  var vsota;
  switch(izvajalec){
    case 'Queen':
      return 0;
    case 'Led Zepplin':
      return 0;
    case 'Kiss':
      return 0;
    case 'Justin Bieber':
      return 22;
    default:
     vsota =9.5;
    break;
  }
  
  switch(zanr){
    case 'Metal':
      return 0;
    case 'Heavy Metal':
      return 0;

    case 'Easy Listening':
      return 0;

    default:
      return 9.5;

  }
  
  return vsota;
};


// Vrni podrobnosti pesmi v košarici iz podatkovne baze
var pesmiIzKosarice = function(zahteva, callback) {
  if (!zahteva.session.kosarica || zahteva.session.kosarica.length == 0) {
    callback([]);
  } else {
    var vrstice =pb.all("SELECT Track.TrackId AS stevilkaArtikla, 1 AS kolicina, Artist.Name AS izvajalec, Genre.Name AS zanr, \
    Track.Name || ' (' || Artist.Name || ') (' || Genre.Name || ') ' AS opisArtikla, \
    Track.UnitPrice * " + razmerje_usd_eur + " AS cena, 0 AS popust \
    FROM Track, Album, Artist, Genre \
    WHERE Track.AlbumId = Album.AlbumId AND \
    Track.GenreId = Genre.GenreId AND\
    Artist.ArtistId = Album.ArtistId AND \
    Track.TrackId IN (" + zahteva.session.kosarica.join(",") + ")",
    function(napaka, vrstice) {
      if (napaka) {
        callback(false);
      } else {
        for(var i=0; i<vrstice.length; i++){
          //var deli = vrstice[i].split([0]+' ('+[1]+ ') ('+[2]+ ') ');
          vrstice[i].stopnja = davcnaStopnja(vrstice[i].izvajalec, vrstice[i].zanr);
        }
        callback(vrstice);
      }
    });
    
  }
};

streznik.get('/kosarica', function(zahteva, odgovor) {
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi)
      odgovor.sendStatus(500);
    else
      odgovor.send(pesmi);
  });
})

// Izpis računa v HTML predstavitvi ali izvorni XML obliki
streznik.get('/izpisiRacun/:oblika', function(zahteva, odgovor) {
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi) {
      odgovor.sendStatus(500);
    } else if (pesmi.length == 0) {
      odgovor.send("<p>V košarici nimate nobene pesmi, \
        zato računa ni mogoče pripraviti!</p>");
    } else {
      odgovor.setHeader('content-type', 'text/xml');
      odgovor.render('eslog', {
        vizualiziraj: zahteva.params.oblika == 'html' ? true : false,
        postavkeRacuna: pesmi
      })
    }
  })
})

// Privzeto izpiši račun v HTML obliki
streznik.get('/izpisiRacun', function(zahteva, odgovor) {
  odgovor.redirect('/izpisiRacun/html')
})

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik pognan!");
})
